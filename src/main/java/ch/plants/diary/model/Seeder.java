package ch.plants.diary.model;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

public interface Seeder {
    void seed();
}

@Component
class SeederImpl {
    private final List<Seeder> seeders;

    SeederImpl(List<Seeder> seeders) {
        this.seeders = seeders;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        for (Seeder seeder : seeders) {
            seeder.seed();
        }
    }
}