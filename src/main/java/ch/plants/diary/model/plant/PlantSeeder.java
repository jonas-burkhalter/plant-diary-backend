package ch.plants.diary.model.plant;

import ch.plants.diary.model.Seeder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@ToString
@RequiredArgsConstructor

@Order(20)
@Component
public class PlantSeeder implements Seeder {
    private static final List<Plant> PLANTS = List.of(
            new Plant(1L, "Basil", 1L, 1L)
    );

    private final PlantRepository plantRepository;

    public void seed() {
        if (plantRepository.count() <= 0) {
            plantRepository.saveAll(PLANTS);
        }
    }
}
