package ch.plants.diary.model.plant;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor

@CrossOrigin("*")
@RestController
@RequestMapping("/plant")
public class PlantResource {
    private final PlantRepository plantRepository;

    @PostMapping
    public Long create(@RequestBody Plant plant) {
        plantRepository.save(plant);
        return plant.getId();
    }


    @GetMapping
    public List<Plant> getAll() {
        return plantRepository.findAll();
    }

    @GetMapping("/{id}")
    public Plant get(@PathVariable Long id) {
        return plantRepository.findById(id).get();
    }
}
