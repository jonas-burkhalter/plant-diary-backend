package ch.plants.diary.model.pot;

import ch.plants.diary.model.Seeder;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@ToString
@RequiredArgsConstructor

@Order(10)
@Component
public class PotSeeder implements Seeder {
    private static final List<Pot> POTS = List.of(
            new Pot(1L, "Fensterbank (Küche)"),
            new Pot(2L, "Fensterbank (Weinregal)"),
            new Pot(3L, "Fensterbank (Wohnzimmer)"),
            new Pot(4L, "Fensterbank (Fernseher)"),
            new Pot(5L, "Fensterbank (Eric)"),
            new Pot(6L, "Fensterbank (Luis)"),
            new Pot(7L, "Fensterbank (Luis)"),
            new Pot(8L, "Fensterbank (Jonas)")
    );

    private final PotRepository potRepository;

    public void seed() {
        if (potRepository.count() <= 0) {
            potRepository.saveAll(POTS);
        }
    }

}
