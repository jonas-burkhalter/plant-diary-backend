package ch.plants.diary.model.pot;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "pot", path = "pot")
public interface PotRepository extends JpaRepository<Pot, Long> {
}
