package ch.plants.diary.model.pot;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Pot {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String place;
}
