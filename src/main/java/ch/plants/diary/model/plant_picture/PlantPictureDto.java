package ch.plants.diary.model.plant_picture;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class PlantPictureDto {
    private Long id;
    private byte[] data;
    private String filename;
}
