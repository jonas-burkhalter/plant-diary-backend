package ch.plants.diary.model.plant_picture;

import java.io.IOException;

public interface PlantPictureAdapter {
    void create(PlantPicture entity, byte[] file) throws IOException;

    void delete(PlantPicture entity) throws IOException;

    byte[] get(PlantPicture entity) throws IOException;

    void update(PlantPicture entity, byte[] file) throws IOException;
}
