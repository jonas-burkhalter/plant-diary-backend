package ch.plants.diary.model.plant_picture;

import ch.plants.diary.model.Seeder;
import ch.plants.diary.model.plant.Plant;
import ch.plants.diary.model.plant.PlantRepository;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@ToString
@RequiredArgsConstructor

@Order(30)
@Component
public class PlantPictureSeeder implements Seeder {
    private static final List<PlantPicture> PLANT_PICTURES = List.of(
            new PlantPicture(1L, "basil.png", 1L)
    );

    private final PlantPictureRepository plantPictureRepository;

    public void seed() {
        if (plantPictureRepository.count() <= 0) {
            plantPictureRepository.saveAll(PLANT_PICTURES);
        }
    }
}
