package ch.plants.diary.model.plant_picture;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Repository
public class PlantPictureFilesystemAdapter implements PlantPictureAdapter {
    String RESOURCES_DIR = PlantPictureFilesystemAdapter.class.getResource("/").getPath();

    @Override
    public void create(PlantPicture entity, byte[] file) throws IOException {
        Path filepath = getFilepath(entity);
        Files.createDirectories(filepath.getParent());

        Files.write(filepath, file);
    }

    @Override
    public void delete(PlantPicture entity) throws IOException {
        Path filepath = getFilepath(entity);
        Files.deleteIfExists(filepath);
    }

    @Override
    public byte[] get(PlantPicture entity) throws IOException {
        Path filepath = getFilepath(entity);
        String location = filepath.toAbsolutePath().toString();
        FileSystemResource fileSystemResource = new FileSystemResource(Paths.get(location));
        return fileSystemResource.getContentAsByteArray();
    }

    @Override
    public void update(PlantPicture entity, byte[] file) throws IOException {
        delete(entity);
        create(entity, file);
    }

    private Path getFilepath(PlantPicture entity) {
        return Paths.get(RESOURCES_DIR + "plants/" + entity.getPlantId() + "/" + entity.getId());
    }
}
