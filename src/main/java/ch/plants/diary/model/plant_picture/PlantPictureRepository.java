package ch.plants.diary.model.plant_picture;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantPictureRepository extends JpaRepository<PlantPicture, Long> {
    List<PlantPicture> findAllByPlantId(Long plantId);
}
