package ch.plants.diary.model.plant_picture;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class PlantPicture {
    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    private String filename;

    @NotNull
    private Long plantId;
}
