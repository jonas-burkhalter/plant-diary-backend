package ch.plants.diary.model.plant_picture;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor

@CrossOrigin("*")
@RestController
@RequestMapping("/plant/{plantId}/picture")
public class PlantPictureResource {
    private final PlantPictureAdapter plantPictureAdapter;
    private final PlantPictureRepository plantPictureRepository;

    @PostMapping
    public Long create(@PathVariable Long plantId, @RequestBody PlantPictureDto plantPictureDto) throws IOException {
        PlantPicture plantPicture = new PlantPicture(null, plantPictureDto.getFilename(), plantId);
        plantPictureRepository.save(plantPicture);

        plantPictureAdapter.create(plantPicture, plantPictureDto.getData());

        return plantPicture.getId();
    }

    @GetMapping
    public List<PlantPictureDto> get(@PathVariable Long plantId) throws IOException {
        List<PlantPicture> plantPictures = plantPictureRepository.findAllByPlantId(plantId);

        List<PlantPictureDto> plantPictureDtos = new ArrayList<>();

        for (PlantPicture plantPicture : plantPictures) {
            PlantPictureDto plantPictureDto = new PlantPictureDto();

            plantPictureDto.setId(plantPicture.getId());
            plantPictureDto.setFilename(plantPicture.getFilename());

            byte[] bytes = plantPictureAdapter.get(plantPicture);
            plantPictureDto.setData(bytes);

            plantPictureDtos.add(plantPictureDto);
        }

        return plantPictureDtos;
    }
}
